# farfetch_ds_summit_2021_experimentation

Example of AB test sequential methodology.  
   
Pappers describing misture Sequential Probability Ration Test (mSPRT):  
 *  [stats_engine_technical_paper](https://gitlab.com/joao_fidalgo/farfetch_ds_summit_2021_experimentation/-/blob/main/stats_engine_technical_paper.pdf)  
 *  [Peeking at AB Tests Why it matters and what to do about it](https://gitlab.com/joao_fidalgo/farfetch_ds_summit_2021_experimentation/-/blob/main/Peeking%20at%20AB%20Tests%20Why%20it%20matters%20and%20what%20to%20do%20about%20it.pdf)  
   
Example implementation of mSPRT:
*  [experiment](https://gitlab.com/joao_fidalgo/farfetch_ds_summit_2021_experimentation/-/blob/main/experiment.py)  
  
Step by step explanation of mSPRT:
*  [sequential_methodology_step_by_step](https://gitlab.com/joao_fidalgo/farfetch_ds_summit_2021_experimentation/-/blob/main/sequential_methodology_step_by_step.ipynb)
  
Auxiliar code to generate experiments input data (random data) to run examples and simulations:  
*  [simulation](https://gitlab.com/joao_fidalgo/farfetch_ds_summit_2021_experimentation/-/blob/main/simulation.py)  
  
Example of simulation:  
*  [simulation_example](https://gitlab.com/joao_fidalgo/farfetch_ds_summit_2021_experimentation/-/blob/main/simulation_example.ipynb)

Enjoy! =)



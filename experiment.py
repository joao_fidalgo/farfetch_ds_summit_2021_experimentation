import numpy as np
import pandas as pd
from scipy.stats import norm


class Exp:
    """
    Class to calculate experiment results (AB Test)
    
    Attributes
    ----------
    control: dict
        Initial values and calculations needed of the control.
    variant: dict
        Initial values and calculations needed of the variant.
    exp: dict
        Calculations and outputs of the experiment.
    output: pd.DataFrame
        A table with the result and intermidiate calculations of the experiment.
    days: np.array
        The index of the days.
    exp_name: str
        The name of the experiment.
    prior_tau: float
        The prior tau is a parameter that should be adjusted in funciton of 
        the expected values data used in experiments.
    alpha: float
        Alpha (level of false positives desired)
    min_days: int
        Min #days before experiment can reach significance.
    max_days: int
        Max #days before experiment is declared inconclusive 
        if it does not have a decsion yet.
    
    
    Methods
    -------
    process()
        Processes the experiment. 
    """
    
    def __init__(self, observs_control: np.array, convers_control: np.array, observs_variant: np.array,
                 convers_variant: np.array, prior_tau: float, alpha: float, min_days: int, max_days: int,
                 exp_name: str = 'None'):
        """
        Parameters
        ----------
        observs_control : np.array
            Observations control
        convers_control : np.array
            Conversions control
        observs_variant : np.array
            Observations variant
        convers_variant : np.array
            Conversions variant
        exp_name: str
            The name of the experiment
        prior_tau: float
            The prior tau is a parameter that should be adjusted in funciton of 
            the expected values data used in experiments.
        alpha: float
            Alpha (level of false positives desired)
        min_days: int
            Min #days before experiment can reach significance.
        max_days: int
            Max #days before experiment is declared inconclusive 
            if it does not have a decsion yet.
        """

        self.control = {
            'observs': observs_control,
            'convers': convers_control,
            'cum_observs': None,
            'cum_convers': None,
            'cum_metric_rate': None,
            'variance': None,
        }
        self.variant = {
            'observs': observs_variant,
            'convers': convers_variant,
            'cum_observs': None,
            'cum_convers': None,
            'cum_metric_rate': None,
            'variance': None,
        }
        self.exp = {
            'cum_metric_rate_diff': None,
            'cum_metric_rate_diff_rel': None,
            'tau': None,
            'se2': None,
            'mde': None,
            'mde_rel': None,
            'ci_lower_marg_rel': None,
            'ci_upper_marg_rel': None,
            'ci_lower_cum_rel': None,
            'ci_upper_cum_rel': None,
            'marg_signif': None,
            'cum_signif': None,
            'is_signif': None,
            'decision': None,
            'is_win': None,
        }
        self.output = None
        self.days = range(len(observs_control))
        self.exp_name = exp_name
        self.prior_tau = prior_tau
        self.alpha = alpha
        self.min_days = min_days
        self.max_days = max_days

    def process(self) -> pd.DataFrame:
        """Processes the experiment.

        Returns
        -------
        pd.DataFrame
            (cumumlative observations, cumumlative conversions, cumumlative metric rate)
        """
        for var in [self.control, self.variant]:
            var['cum_observs'], var['cum_convers'], var['cum_metric_rate'] = self.calculate_cum_values(
                var['observs'], var['convers'])
            var['variance'] = self.calculate_variance(var['cum_metric_rate'])

        self.exp['cum_metric_rate_diff'], self.exp['cum_metric_rate_diff_rel'] = self.calculate_diff_variant_control(
            self.control['cum_metric_rate'], self.variant['cum_metric_rate'])
        self.exp['tau'] = self.calculate_tau(self.control['variance'], self.variant['variance'], self.prior_tau)
        self.exp['se2'] = self.calculate_se2(self.control['variance'], self.control['cum_observs'],
                                             self.variant['variance'], self.variant['cum_observs'])
        self.exp['mde'] = self.calculate_mde(self.exp['se2'], self.alpha, self.exp['tau'])
        self.exp['mde_rel'] = self.calculate_mde_rel(self.exp['mde'], self.control['cum_metric_rate'])
        self.exp['ci_lower_marg_rel'], self.exp['ci_upper_marg_rel'] = self.calculate_ci_marginal_rel(
            self.exp['cum_metric_rate_diff_rel'], self.exp['mde_rel'])
        self.exp['ci_lower_cum_rel'], self.exp['ci_upper_cum_rel'] = self.calculate_ci_cum_rel(
            self.exp['ci_lower_marg_rel'], self.exp['ci_upper_marg_rel'], self.exp['cum_metric_rate_diff_rel'],
            self.min_days)
        self.exp['marg_signif'] = self.calculate_marg_signif(self.exp['cum_metric_rate_diff'], self.exp['mde'],
                                                             self.alpha, self.min_days)
        self.exp['cum_signif'] = self.calculate_cum_signif(self.exp['marg_signif'])
        self.exp['is_signif'] = self.calculate_is_signif(self.exp['cum_signif'], self.alpha)
        self.exp['decision'] = self.calculate_decision(
            self.exp['ci_lower_cum_rel'], self.exp['ci_upper_cum_rel'], self.days, self.min_days, self.max_days)
        self.exp['is_win'] = self.calculate_is_win(self.exp['decision'])
        self.output = self.build_output(self.control, self.variant, self.exp, self.exp_name, self.days)
        return self.output

    @staticmethod
    def calculate_cum_values(observs: np.array, convers: np.array) -> (np.array, np.array, np.array):
        """Calculates cumulative observations, conversions and metric rate

        Parameters
        ----------
        observs : np.array
            Marginal observations
        convers : np.array
            Marginal conversions

        Returns
        -------
        (np.array, np.array, np.array)
            (cumumlative observations, cumumlative conversions, cumumlative metric rate)
        """

        np.seterr(all='ignore')
        cum_observs = observs.cumsum()
        cum_convers = convers.cumsum()
        cum_metric_rate = cum_convers / cum_observs
        cum_metric_rate = np.where(np.isinf(cum_metric_rate), np.NaN, cum_metric_rate)
        return cum_observs, cum_convers, cum_metric_rate

    @staticmethod
    def calculate_variance(cum_metric_rate: np.array) -> np.array:
        """Calculates variance

        Parameters
        ----------
        cum_metric_rate : np.array
            Cumulative metric rate

        Returns
        -------
        np.array
            Variance
        """
        variance = cum_metric_rate * (1 - cum_metric_rate)
        return variance

    @staticmethod
    def calculate_diff_variant_control(control_cum_metric_rate: np.array, variant_cum_metric_rate: np.array
                                      ) -> (np.array, np.array):
        """Calculates metric rate difference between variant and control

        Parameters
        ----------
        control_cum_metric_rate : np.array
            Control metric rate 
        variant_cum_metric_rate : np.array
            Variant metric rate 

        Returns
        -------
        (np.array, np.array)
            Cumulative metric rate difference,
            Cumulative metric rate difference relative to control metric rate
        """

        np.seterr(all='ignore')
        cum_metric_rate_diff = variant_cum_metric_rate - control_cum_metric_rate
        cum_metric_rate_diff_rel = cum_metric_rate_diff / control_cum_metric_rate
        cum_metric_rate_diff_rel = np.where(np.isinf(cum_metric_rate_diff_rel), np.NaN, cum_metric_rate_diff_rel)
        return cum_metric_rate_diff, cum_metric_rate_diff_rel

    @staticmethod
    def calculate_tau(control_variance: np.array, variant_variance: np.array, prior_tau: float) -> np.array:
        """

        Parameters
        ----------
        control_variance : np.array
            Control variance
        variant_variance : np.array
            Variant variance
        prior_tau: float
            The prior tau is a parameter that should be adjusted in funciton of 
            the expected values data used in experiments.

        Returns
        -------
        np.array
            Tau is a intermidiate value used for the calculation 
            of the MDE (minimum detectable effect).
        """
        
        tau = (control_variance + variant_variance) * prior_tau
        return tau

    @staticmethod
    def calculate_se2(control_variance: np.array, control_cum_observs: np.array, 
                      variant_variance: np.array, variant_cum_observs: np.array) -> np.array:
        """Calculates the squared standard error

        Parameters
        ----------
        control_variance : np.array
            Control variance
        variant_variance : np.array
            Variant variance

        Returns
        -------
        np.array
            Squared standard error 
        """
        
        np.seterr(all='ignore')
        control_se2 = control_variance / control_cum_observs
        variant_se2 = variant_variance / variant_cum_observs
        se2 = control_se2 + variant_se2
        return se2

    @staticmethod
    def calculate_mde(se2: np.array, alpha: float, tau: np.array) -> np.array:
        """Calculates the minimum detectable effect.

        Parameters
        ----------
        se2: np.array
            Squared standard error
        alpha: float
            Alpha
        tau: np.array
            Tau is a intermidiate value used for the calculation 
            of the MDE (minimum detectable effect).

        Returns
        -------
        np.array
            Minimum detectable effect. 
            The minimum difference between variant and control 
            needed for the expleriment to be conclusive (win/lose).
        """
    
        np.seterr(all='ignore')
        mde = np.sqrt(se2 * (se2 + tau) / tau * np.log((se2 + tau) / se2 * (1 / alpha) ** 2))
        return mde

    @staticmethod
    def calculate_mde_rel(mde: np.array, control_cum_metric_rate: np.array):
        """Calculates the minimum detectable effect relative to control metric rate

        Parameters
        ----------
         : np.array


        Returns
        -------
        np.array
            Minimum detectable effect relative to control metric rate. 
        """

        np.seterr(all='ignore')
        mde_rel = mde / control_cum_metric_rate
        mde_rel = np.where(np.isinf(mde_rel), np.NaN, mde_rel)
        return mde_rel

    @staticmethod
    def calculate_ci_marginal_rel(cum_metric_rate_diff_rel: np.array, mde_rel: np.array) -> (np.array, np.array):
        """Calculates the confidence interval.

        Parameters
        ----------
        cum_metric_rate_diff_rel: np.array
            Metric rate difference between variant and control relative to control metric rate
        mde_rel: np.array
            Minimum detectable effect relative to control metric rate. 

        Returns
        -------
        (np.array, np.array)
            Confidence interval
        """
    
        center = cum_metric_rate_diff_rel
        width = mde_rel
        ci_lower_marg_rel = center - width
        ci_upper_marg_rel = center + width
        return ci_lower_marg_rel, ci_upper_marg_rel

    @staticmethod
    def marg_to_cum(series: np.array, min_index: int, func: str):
        """Calculates the cumulative func of an array.

        Parameters
        ----------
        series : np.array
            The array with marginal values to calculate cumulative values on.
        min_index:
            The return of this function will be an array 
            where until the min_index the values are going to be marginal 
            and after the min_index the values are going to be cumulative.
        func: str
            The function to calculate cum vales ex: 'mincum', 'maxcum'

        Returns
        -------
        np.array
            Cumulative values array
        """

        min_index = int(min_index)
        series = pd.Series(series)
        fixed_series = series[:min_index]
        changed_series = series[min_index:].apply(func)
        cum_series = pd.concat([fixed_series, changed_series]).values
        return cum_series

    def calculate_ci_cum_rel(self, ci_lower_marg_rel: np.array, ci_upper_marg_rel: np.array, 
                             cum_metric_rate_diff_rel: np.array, min_days: int
                            ) -> (np.array, np.array):
        """Calculates the confidence interval cumulative relative

        Parameters
        ----------
        ci_lower_marg_rel : np.array
            Confidence interval marginal relative lower boundary
        ci_lower_marg_rel : np.array
            Confidence interval marginal relative upper boundary
        cum_metric_rate_diff_rel: np.array
            Metric rate difference between variant and control
            relative to control metric rate

        Returns
        -------
        (np.array, np.array)
            Confidence interval cumulative relative
        """
        ci_lower_cum_rel = self.marg_to_cum(ci_lower_marg_rel, min_days, 'cummax')
        ci_upper_cum_rel = self.marg_to_cum(ci_upper_marg_rel, min_days, 'cummin')
        center = cum_metric_rate_diff_rel

        "Reset lower and upper limit whenerver the metric rate difference is outside the confidence interval"
        while max(ci_upper_cum_rel <= center):
            old_ci = ci_upper_cum_rel.copy()
            idx = np.argmax(ci_upper_cum_rel <= center)
            min_index = idx - min_days
            ci_upper_cum_rel[idx:] = self.marg_to_cum(ci_upper_marg_rel[idx:], min_index, 'cummin')
            if np.array_equal(old_ci, ci_upper_cum_rel, equal_nan=True):
                break

        while max(ci_lower_cum_rel >= center):
            old_ci = ci_lower_cum_rel.copy()
            idx = np.argmax(ci_lower_cum_rel >= center)
            min_index = idx - min_days
            ci_lower_cum_rel[idx:] = self.marg_to_cum(ci_lower_marg_rel[idx:], min_index, 'cummax')
            if np.array_equal(old_ci, ci_lower_cum_rel, equal_nan=True):
                break

        return ci_lower_cum_rel, ci_upper_cum_rel

    @staticmethod
    def calculate_marg_signif(cum_metric_rate_diff: np.array, mde: np.array, 
                              alpha: float, min_days: int) -> np.array:
        """

        Parameters
        ----------
        cum_metric_rate_diff: np.array
            Metric rate difference between variant and control 
        mde: np.array
            Minimum detectable effect
        alpha: float
            Alpha
        min_days: int
            Min #days before experiment can reach significance        

        Returns
        -------
        np.array
            Marginal significance
        """
        
        np.seterr(all='ignore')
        loc = abs(cum_metric_rate_diff)
        loc = np.nan_to_num(loc, 0)
        normal_deviation = norm.ppf(1 - alpha / 2)
        scale = mde / normal_deviation
        scale = np.nan_to_num(scale, np.inf)
        scale = np.where(scale == 0, np.inf, scale)
        marg_signif = 1 - 2 * norm.cdf(0, loc=loc, scale=scale)
        marg_signif[:min_days] = 0
        return marg_signif

    def calculate_cum_signif(self, marg_signif: np.array) -> np.array:
        """

        Parameters
        ----------
        marg_signif : np.array
            Marginal significnace

        Returns
        -------
        np.array
            Cumulative significance
        """
        
        cum_signif = self.marg_to_cum(marg_signif, 0, 'cummax')
        return cum_signif

    @staticmethod
    def calculate_is_signif(cum_signif: np.array, alpha: float) -> np.array:
        """Calulcates if experiment is significant

        Parameters
        ----------
        cum_signif: np.array
            Cumulative Significance
        alpha: float
            Alpha
        day: np.array
            the index of the days

        Returns
        -------
        np.array
            Is experiment significant?
        """
        
        is_signif = cum_signif > (1 - alpha)
        return is_signif

    @staticmethod
    def calculate_is_win(decision):
        is_win = decision == 'win'
        return is_win

    @staticmethod
    def apply_decision(x: pd.Series, min_days: int, max_days: int) -> str:
        """Calulcates decision of the experiment on a given day

        Parameters
        ----------
        x : pd.Series(columns = ['day', 'ci_lower_cum_rel', 'ci_upper_cum_rel'])
            Values needed to calculate decision
        min_days: int
            Min #days before experiment can reach significance.
        max_days: int
            Max #days before experiment is declared inconclusive 
            if it does not have a decsion yet.

        Returns
        -------
        str
            Decision
        """
        if x['day'] < min_days:
            return 'still_running'
        elif x['ci_lower_cum_rel'] >= 0:
            return "win"
        elif x['ci_upper_cum_rel'] <= 0:
            return "lose"
        elif x['day'] >= max_days:
            return "inconclusive"
        else:
            return 'still_running'

    def calculate_decision(self, ci_lower_cum_rel: np.array, ci_upper_cum_rel: np.array, 
                           day: np.array, min_days: int, max_days: int) -> np.array:
        """Calulcates decision of the experiment

        Parameters
        ----------
        ci_lower_marg_rel: np.array
            Confidence interval marginal relative lower boundary
        ci_lower_marg_rel: np.array
            Confidence interval marginal relative upper boundary
        day: np.array
            the index of the days
        min_days: int
            Min #days before experiment can reach significance.
        max_days: int
            Max #days before experiment is declared inconclusive 
            if it does not have a decsion yet.

        Returns
        -------
        np.array
            Decision
        """
        
        df = pd.DataFrame({'day': day, 'ci_lower_cum_rel': ci_lower_cum_rel, 'ci_upper_cum_rel': ci_upper_cum_rel})
        decision = df.apply(self.apply_decision, min_days=min_days, max_days=max_days, axis=1)
        return decision

    @staticmethod
    def build_output(control, variant, exp, exp_name, days):
        output = pd.DataFrame({
            'exp_name': [exp_name] * len(days),
            'day': days,
            'observs_control': control['observs'],
            'convers_control': control['convers'],
            'cum_observs_control': control['cum_observs'],
            'cum_convers_control': control['cum_convers'],
            'cum_metric_rate_control': control['cum_metric_rate'],
            'variance_control': control['variance'],
            'observs_variant': variant['observs'],
            'convers_variant': variant['convers'],
            'cum_observs_variant': variant['cum_observs'],
            'cum_convers_variant': variant['cum_convers'],
            'cum_metric_rate_variant': variant['cum_metric_rate'],
            'variance_variant': variant['variance'],
            'cum_metric_rate_diff': exp['cum_metric_rate_diff'],
            'cum_metric_rate_diff_rel': exp['cum_metric_rate_diff_rel'],
            'tau': exp['tau'],
            'se2': exp['se2'],
            'mde': exp['mde'],
            'mde_rel': exp['mde_rel'],
            'ci_lower_marg_rel': exp['ci_lower_marg_rel'],
            'ci_upper_marg_rel': exp['ci_upper_marg_rel'],
            'ci_lower_cum_rel': exp['ci_lower_cum_rel'],
            'ci_upper_cum_rel': exp['ci_upper_cum_rel'],
            'marg_signif': exp['marg_signif'],
            'cum_signif': exp['cum_signif'],
            'is_signif': exp['is_signif'],
            'decision': exp['decision'],
            'is_win': exp['is_win'],
        })
        return output

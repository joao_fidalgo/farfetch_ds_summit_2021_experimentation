import numpy as np
import pandas as pd
from experiment import Exp
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context('talk')
sns.set_style('darkgrid')


def generate_random_inputs_binary(
    n_exp: int, n_days: int, obs_day: int, metric_rate_control: float, variant_distribution: float,
    uplift: float, prior_tau: float, alpha: float, min_days: int = 0, max_days: int = None,
    percent_noise: float = 0, percent_noise_no_increase_obs: float = 0, funnel_metric_rate: float = None, seed=0):

    max_days = max_days if max_days is not None else n_days

    obs_day_control = obs_day * (1 - percent_noise_no_increase_obs) * variant_distribution
    obs_day_variant = obs_day * (1 - percent_noise_no_increase_obs) * (1 - variant_distribution)
    if percent_noise > 0:
        obs_day_noise = (obs_day * percent_noise) / (1 - percent_noise)
    else:
        obs_day_noise = obs_day * percent_noise_no_increase_obs
    obs_day_noise_control = obs_day_noise * variant_distribution
    obs_day_noise_variant = obs_day_noise * (1 - variant_distribution)

    metric_rate_variant = metric_rate_control * (1 + uplift)
    total_obs_day_control = obs_day_control + obs_day_noise_control
    total_obs_day_variant = obs_day_variant + obs_day_noise_variant
    observs_control = np.repeat(total_obs_day_control, n_days)
    observs_variant = np.repeat(total_obs_day_variant, n_days)

    np.random.seed(seed)
    convers_control_real_effect = np.random.binomial(obs_day_control, metric_rate_control, (n_exp, n_days))
    convers_variant_real_effect = np.random.binomial(obs_day_variant, metric_rate_variant, (n_exp, n_days))
    convers_control_noise = np.random.binomial(obs_day_noise_control, metric_rate_control, (n_exp, n_days))
    convers_variant_noise = np.random.binomial(obs_day_noise_variant, metric_rate_control, (n_exp, n_days))

    convers_control = convers_control_real_effect + convers_control_noise
    convers_variant = convers_variant_real_effect + convers_variant_noise

    if funnel_metric_rate == 0:
        convers_control = convers_control * 0
        convers_variant = convers_variant * 0
    elif funnel_metric_rate is not None:
        convers_control = np.random.binomial(convers_control, funnel_metric_rate)
        convers_variant = np.random.binomial(convers_variant, funnel_metric_rate)
    np.random.seed(None)

    list_inputs_kwargs = [
        {
            'exp_name': i,
            'observs_control': observs_control,
            'convers_control': convers_control[i],
            'observs_variant': observs_variant,
            'convers_variant': convers_variant[i],
            'prior_tau': prior_tau,
            'alpha': alpha,
            'min_days': min_days,
            'max_days': max_days,
        }
        for i in range(n_exp)
    ]
    return list_inputs_kwargs


def run_simulation(n_exp: int, n_days: int, obs_day: int, metric_rate_control: float, variant_distribution: float,
                   uplift: float, prior_tau: float, alpha: float, min_days: int = 0, max_days: int = None,
                   percent_noise: float = 0, percent_noise_no_increase_obs: float = 0, funnel_metric_rate: float = None,
                   seed=0):

    if max_days is None:
        max_days = n_days
    list_inputs_kwargs = generate_random_inputs_binary(
        n_exp, n_days, obs_day, metric_rate_control, variant_distribution, uplift, prior_tau, alpha, min_days,
        max_days, percent_noise, percent_noise_no_increase_obs, funnel_metric_rate, seed)
    list_exps_output = list()
    for inputs_kwargs in list_inputs_kwargs:
        exp_output = Exp(**inputs_kwargs).process()
        list_exps_output.append(exp_output)
    exps_output = pd.concat(list_exps_output)
    return exps_output


def calculate_percent_is_win(n_exp: int, n_days: int, obs_day: int, metric_rate_control: float,
                             variant_distribution: float, uplift: float, prior_tau: float, alpha: float,
                             min_days: int = 0, max_days: int = None, percent_noise: float = 0,
                             percent_noise_no_increase_obs: float = 0, funnel_metric_rate: float = None, seed=0):
    exps_output = run_simulation(n_exp, n_days, obs_day, metric_rate_control, variant_distribution, uplift, prior_tau,
                                 alpha, min_days, max_days, percent_noise, percent_noise_no_increase_obs,
                                 funnel_metric_rate, seed)
    percent_is_win = exps_output.groupby('exp_name').is_win.max().sum() / n_exp
    return percent_is_win


def draw_chart(study_name, df_study, parameter_readable_name, list_values_parameter):
    title = f'% of conclusive experiments by {parameter_readable_name}'
    xlim = (list_values_parameter[0], list_values_parameter[-1])
    ax = df_study.plot(figsize=(7, 7), title=title, ylim=(0, 1), xlim=xlim)
    vals_x = ax.get_xticks()
    _ = ax.xaxis.set_ticks(vals_x)
    xticklabels = ['{:,.0%}'.format(x) for x in vals_x]
    if xticklabels[0] == xticklabels[1]:
        xticklabels = ['{:,.1%}'.format(x) for x in vals_x]
    _ = ax.set_xticklabels(xticklabels)
    vals_y = ax.get_yticks()
    _ = ax.yaxis.set_ticks(vals_y)
    _ = ax.set_yticklabels(['{:,.0%}'.format(y) for y in vals_y])
    _ = plt.savefig(f'{study_name}.png', dpi=199)
    return ax


def study_parameter(study_name: str, parameter: str, list_values_parameter: list, parameter_readable_name: str,
                    n_exp: int, n_days: int, obs_day: int, metric_rate_control: float, variant_distribution: float,
                    uplift: float, prior_tau: float, alpha: float, min_days: int, max_days: int = None,
                    percent_noise: float = 0, percent_noise_no_increase_obs: float = 0,
                    funnel_metric_rate: float = None, seed=0, verbose=False):
    kwargs = {'n_exp': n_exp, 'n_days': n_days, 'obs_day': obs_day, 'metric_rate_control': metric_rate_control,
              'variant_distribution': variant_distribution, 'uplift': uplift, 'prior_tau': prior_tau, 'alpha': alpha,
              'min_days': min_days, 'max_days': max_days, 'percent_noise': percent_noise,
              'percent_noise_no_increase_obs': percent_noise_no_increase_obs, 'funnel_metric_rate': funnel_metric_rate,
              'seed': seed}

    list_percent_is_win = list()
    for value in list_values_parameter:
        if verbose:
            print(f'value: {value}')
        _kwargs = {**{parameter: value}, **{k: v for k, v in kwargs.items() if k != parameter}}.copy()
        percent_is_win = calculate_percent_is_win(**_kwargs)
        list_percent_is_win.append(percent_is_win)
    df_study = pd.DataFrame({
        parameter_readable_name: list_values_parameter,
        '% of conclusive experiments': list_percent_is_win
    })
    df_study.set_index(parameter_readable_name, inplace=True)
    df_study.to_pickle(study_name)
    draw_chart(study_name, df_study, parameter_readable_name, list_values_parameter)
    return df_study

